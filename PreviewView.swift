//
//  PreviewView.swift
//
//  Created by Will Boland on 11/9/19.
//

import UIKit
import AVFoundation

/**
 The PreviewView that should be used with VideoCapture.swift.
 */
class PreviewView: UIView {
  override class var layerClass: AnyClass {
    return AVCaptureVideoPreviewLayer.self
  }
  
  ///Convenience wrapper to get layer as its statically known type.
  var videoPreviewLayer: AVCaptureVideoPreviewLayer {
    return layer as! AVCaptureVideoPreviewLayer
  }
}
