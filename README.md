# **Video Capture for iOS** #

## **Overview** ##

There are two files you will need:  

- **VideoCapture.swift**: Provides the main interface for easy video capture and recording to Camera Roll.  
- **PreviewView.swift:** A helper UIView that makes the PreviewLayer easier. Use this in Storyboard for where you want the user to see the camera.
  
## **Capturing Video** ##
```swift
//Initialize the VideoCapture
let videoCapture = VideoCapture(with: myPreviewView)
//Setup the camera with any device type, media type, and position.
videoCapture.setupCamera(withCamera: .builtInWideAngleCamera, for: .video, position: .back) //Setsup camera for wide angle video using the rear camera
//start the camera
videoCapture.startCamera()
```  

## **Recording Video and Saving to Camera Roll** ##
```swift
//To start recording to eventually save to the camera roll
videoCapture.startRecording()
//To stop recording and save to the CameraRoll
videoCapture.stopRecording()
```