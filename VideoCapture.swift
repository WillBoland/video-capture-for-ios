//
//  VideoCapture.swift
//
//  Created by Will Boland on 11/9/19.
//  Copyright © 2019 Will Boland. All rights reserved.
//

import UIKit
import AVFoundation
import Photos

//--------------------
//MARK: Video Capture Class
/**
 Provides an easy-to-use class for recording and saving video on iOS.
 */
class VideoCapture: NSObject, AVCaptureFileOutputRecordingDelegate {
  
  //--------------------
  //MARK: Variables and Constants
  private let captureSession: AVCaptureSession!
  private let previewView: PreviewView!
  private let movieOutput: AVCaptureMovieFileOutput!
  private var movieOutputURL: URL?
  
  //--------------------
  //MARK: Initializers
  //----------------------------------------------------------------------------
  /**
   Initializes the camera for a given session, preview, and output.
   - Parameter forSession: the `AVCAptureSession` for which to use for the camera.
   - Parameter with: the `PreviewView` that the user will be able to see the video from the camera.
   - Parameter to: the `AVCaptureMovieFileOutput` that the video can be recorded to.
   */
  init(forSession session: AVCaptureSession, with preview: PreviewView, to output: AVCaptureMovieFileOutput) {
    self.captureSession = session
    self.previewView = preview
    self.movieOutput = output
  }
  
  //----------------------------------------------------------------------------
  /**
  Initializes the camera for a given session, preview, and output.
  - Parameter forSession: the `AVCAptureSession` for which to use for the camera.
  - Parameter with: the `PreviewView` that the user will be able to see the video from the camera.
  */
  convenience init(forSession session: AVCaptureSession, with preview: PreviewView) {
    self.init(forSession: session, with: preview, to: AVCaptureMovieFileOutput())
  }
  
  //----------------------------------------------------------------------------
  /**
  Initializes the camera for a given session, preview, and output.
  - Parameter with: the `PreviewView` that the user will be able to see the video from the camera.
  */
  convenience init(with preview: PreviewView) {
    self.init(forSession: AVCaptureSession(), with: preview, to: AVCaptureMovieFileOutput())
  }
  
  //--------------------
  //MARK: Public Functions
  //----------------------------------------------------------------------------
  /**
  Setsup the camera for a given device and media type, as well as position.
   - Parameter camera: the `AVCaptureDevice.DeviceType` that the camera will use
   - Parameter mediaType: the media type that the capture device will use
   - Parameter position: the position that the camera is in to start the session (front, back)
   - Parameter preset: the capture session preset to use for recording. Default is `.high`.
  */
  func setupCamera(withCamera camera: AVCaptureDevice.DeviceType, for mediaType: AVMediaType?, position: AVCaptureDevice.Position, with preset: AVCaptureSession.Preset = .high) {
    captureSession.beginConfiguration()
    
    //Add inputs for the given parameters
    let videoDevice = AVCaptureDevice.default(camera, for: mediaType, position: position)
    
    guard
      let videoDeviceInput = try? AVCaptureDeviceInput(device: videoDevice!),
      captureSession.canAddInput(videoDeviceInput)
      else { return }
    captureSession.addInput(videoDeviceInput)
    
    //Add outputs, for recording the video
    let videoOutput = AVCaptureVideoDataOutput()
    guard captureSession.canAddOutput(videoOutput) else { return }
    captureSession.sessionPreset = preset
    captureSession.addOutput(videoOutput)
    captureSession.addOutput(movieOutput)
    
    //finished configuring
    captureSession.commitConfiguration()
    previewView.videoPreviewLayer.session = captureSession
    
    //orient correctly, initially based off of orientation of device
    switch UIApplication.shared.statusBarOrientation.rawValue {
    case 1:
      setCameraOrientation(to: .portrait); break;
    case 2:
      setCameraOrientation(to: .portraitUpsideDown); break;
    case 3:
      setCameraOrientation(to: .landscapeRight); break;
    case 4:
      setCameraOrientation(to: .landscapeLeft); break;
    default:
      setCameraOrientation(to: .portrait); break;
    }
    print("Camera setup is complete.")
  }
  
  //----------------------------------------------------------------------------
  /**
  Changes video orientation and output orientation based off of the given video orientation.
   - Parameter to: the orientation to set the video and output to.
  */
  func setCameraOrientation(to orientation: AVCaptureVideoOrientation) {
    previewView.videoPreviewLayer.connection?.videoOrientation = orientation
    
    let movieOutputConnection = movieOutput.connection(with: .video)
    movieOutputConnection?.videoOrientation = orientation
  }
  
  //----------------------------------------------------------------------------
  /**
  Starts running the camera.
  */
  func startCamera() {
    captureSession.startRunning()
    print("Camera started running.")
  }
  
  //----------------------------------------------------------------------------
  /**
  Stops running the camera. Call `startRecording()` to start recording the video to a file.
  */
  func stopCamera() {
    captureSession.stopRunning()
    print("Camera stopped running.")
  }
  
  //----------------------------------------------------------------------------
  /**
  Starts recording to a file. Default file name is the current date. Default type is mov.
   - Parameter withName: the name to be used as the file name. Default is the current date.
   - Parameter ofType: the type of file to be recorded to. (mov, mp4, etc). Default is `mov`.
  */
  func startRecording(withName name: String = "\(Date())", ofType type: String = "mov") {
    let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
    movieOutputURL = paths.first?.appendingPathComponent("\(name).\(type)")
    movieOutput.startRecording(to: movieOutputURL!, recordingDelegate: self as AVCaptureFileOutputRecordingDelegate)
    print("Recording started.")
  }
  
  //----------------------------------------------------------------------------
  /**
  Stops recording the file and saves it to the camera roll.
  */
  func stopRecording() {
    movieOutput.stopRecording()
    print("Recording stopped.")
  }
  
  ///For `AVCaptureFileOutputRecordingDelegate`. Not to be called outside of delegate.
  func fileOutput(_ output: AVCaptureFileOutput, didFinishRecordingTo outputFileURL: URL, from connections: [AVCaptureConnection], error: Error?) {
    saveVideo(toURL: movieOutputURL!)
  }
  
  //--------------------
  //MARK: Private Functions
  //----------------------------------------------------------------------------
  /**
  Saves the video to the given URL.
  */
  private func saveVideo(toURL url: URL) {
    PHPhotoLibrary.shared().performChanges({
      PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: url)
    }) { (success, error) in
      if(success) {
        print("Video saved to Camera Roll.")
      } else {
        print("Video failed to save.")
      }
    }
  }
}
